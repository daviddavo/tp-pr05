package simulator.view;

import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;

import simulator.control.Controller;
import simulator.model.Body;
import simulator.model.SimulatorObserver;

public class StatusBar extends JPanel implements SimulatorObserver {
	private JLabel _currTime;
	private JLabel _currLaws;
	private JLabel _numOfBodies;
	private JProgressBar _progressBar;
	
	StatusBar(Controller ctrl) {
		initGUI();
		ctrl.addObserver(this);
	}
	
	private static JProgressBar createProgressBar() {
		JProgressBar ret = new JProgressBar();
		
		ret.setValue(0);
		ret.setStringPainted(false);
		
		return ret;
	}
	
	private void initGUI() {		
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setBorder(BorderFactory.createBevelBorder(1));
		
		add(new JLabel("Time: "));
		add(_currTime = new JLabel());
		
		add(new JSeparator());
		
		add(new JLabel("Bodies:"));
		add(_numOfBodies = new JLabel());
		
		add(new JSeparator());
		
		add(new JLabel("Laws:"));
		add(_currLaws = new JLabel());
		
		add(new JSeparator());
		_progressBar = createProgressBar();
		add(_progressBar);
	}
	
	/* Package-private */
	JProgressBar getProgressBar() {
		return _progressBar;
	}
	
	private void updateNumOfBodies(int n) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				_numOfBodies.setText(String.format("%2d", n));
			}
		});
	}
	
	private void updateTime(double time) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				_currTime.setText(String.format("%10.4e", time));
			}
		});
	}
	
	private void updateGLaw(String gLawsDesc) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				_currLaws.setText(gLawsDesc);
			}
		});
	}

	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawsDesc) {
		updateNumOfBodies(bodies.size());
		updateTime(time);
		updateGLaw(gLawsDesc);
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawsDesc) {
		onRegister(bodies, time, dt, gLawsDesc);
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		// El body que se ha añadido no nos importa
		updateNumOfBodies(bodies.size());
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		// Los bodies nos dan ingual, porque no se pueden añadir ni eliminar en un advance
		updateTime(time);
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
		// No se usa en el status bar
	}

	@Override
	public void onGravityLawChanged(String gLawsDesc) {
		updateGLaw(gLawsDesc);
	}

}
