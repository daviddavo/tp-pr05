package simulator.view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import simulator.control.Controller;
import simulator.model.Body;
import simulator.model.SimulatorObserver;

public class BodiesTableModel extends AbstractTableModel implements SimulatorObserver {
	private List<Body> _bodies;
	private static final String cNames[] = {"Id", "Mass", "Position", "Velocity", "Acceleration"};
	
	BodiesTableModel(Controller ctrl) {
		_bodies = new ArrayList<>();
		ctrl.addObserver(this);
	}

	@Override
	public int getRowCount() {
		return _bodies.size();
	}

	@Override
	public int getColumnCount() {
		return cNames.length;
	}
	
	@Override
	public String getColumnName(int col) {
		return cNames[col];
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0: return _bodies.get(rowIndex).getId();
		case 1: return _bodies.get(rowIndex).getMass();
		case 2: return _bodies.get(rowIndex).getPosition();
		case 3: return _bodies.get(rowIndex).getVelocity();
		case 4: return _bodies.get(rowIndex).getAcceleration();
		default: return null;
		}
	}
	
	@Override
	public void fireTableDataChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				BodiesTableModel.super.fireTableDataChanged();
			}
		});
	}

	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawsDesc) {
		_bodies = bodies;
		fireTableDataChanged();
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawsDesc) {
		_bodies = bodies;
		fireTableDataChanged();
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		_bodies = bodies;
		fireTableDataChanged();
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		// Actualizamos todos los bodies
		_bodies = bodies;
		fireTableDataChanged();
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
		// La tabla no muestra información sobre el deltatime
	}

	@Override
	public void onGravityLawChanged(String gLawsDesc) {
		// En la tabla no se dice ada de la gravity law
	}

}
