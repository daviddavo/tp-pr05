package simulator.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.text.JTextComponent;

import org.json.JSONObject;

import simulator.control.Controller;
import simulator.factories.Factory;
import simulator.model.Body;
import simulator.model.GravityLaws;
import simulator.model.SimulatorObserver;

public class ControlPanel extends JToolBar implements SimulatorObserver, ActionListener {
	
	// private Thread _thread;
	private SwingWorker<Integer, Integer> _worker;
	private Controller _ctrl;
	private static final String ICONS_DIR = "resources/icons/";
	private static final Integer DEFAULT_STEPS = 10000;
	private static final int SEPARATOR_PADDING = 20;
	private String _currentGravityLaw;
	
	private JButton _openButton;
	private JButton _gLawButton;
	private JButton _runButton;
	private JButton _stopButton;
	private JButton _exitButton;
	
	private JTextComponent _dtField;
	private JSpinner _delayField;
	private JSpinner _stepsField;
	
	private JProgressBar _progressBar = null;
	
	ControlPanel(Controller ctrl) {
		super("Panel de Control");
		_ctrl = ctrl;
		initGUI();
		_ctrl.addObserver(this);
	}
	
	ControlPanel(Controller ctrl, JProgressBar progressBar) {
		this(ctrl);
		_progressBar = progressBar;
	}
	
	private JButton createButton(String imageFileName, String toolTipText) {
		JButton ret = new JButton();
		ret.setIcon(new ImageIcon(ICONS_DIR+imageFileName));
		ret.setToolTipText(toolTipText);
		ret.addActionListener(this);
		
		return ret;
	}
	
	private JPanel createDelayInput() {
		JPanel ret = new JPanel();
		ret.setToolTipText("Delay between steps (milliseconds)");
		ret.setLayout(new BoxLayout(ret, BoxLayout.X_AXIS));
		
		JLabel label = new JLabel("Delay:");
		_delayField = new JSpinner(new SpinnerNumberModel(1, 0, 100, 1));
		
		int h = _delayField.getFontMetrics(_delayField.getFont()).getHeight() + 10;
		int w = _delayField.getFontMetrics(_delayField.getFont()).getMaxAdvance()*5;
		
		_delayField.setPreferredSize(new Dimension(w, h));
		_delayField.setMaximumSize(new Dimension(w*2, h));
		_delayField.setMinimumSize(new Dimension(w, h));
		
		ret.add(label);
		ret.add(_delayField);
		
		return ret;
	}
	
	private JPanel createStepsInput() {
		JPanel ret = new JPanel();
		ret.setToolTipText("Number of steps to run");
		ret.setLayout(new BoxLayout(ret, BoxLayout.X_AXIS));
		
		JLabel label = new JLabel("Steps:");
		_stepsField = new JSpinner(new SpinnerNumberModel(DEFAULT_STEPS, 1, null, 100));
		
		int h = _stepsField.getFontMetrics(_stepsField.getFont()).getHeight() + 10;
		int w = _stepsField.getFontMetrics(_stepsField.getFont()).getMaxAdvance()*5;
		
		_stepsField.setPreferredSize(new Dimension(w, h));
		_stepsField.setMaximumSize(new Dimension(w*2, h));
		_stepsField.setMinimumSize(new Dimension(w, h));
		
		ret.add(label);
		ret.add(_stepsField);
		
		return ret;
	}
	
	private JPanel createDeltaTimeInput() {
		JPanel ret = new JPanel();
		ret.setToolTipText("Time to run each step");
		ret.setLayout(new BoxLayout(ret, BoxLayout.X_AXIS));
		
		JLabel label = new JLabel("Delta-time:");
		_dtField = new JTextField("0");
		int h = _dtField.getFontMetrics(_dtField.getFont()).getHeight() + 10;
		int w = _dtField.getFontMetrics(_dtField.getFont()).getMaxAdvance()*5;
		// _dtField.setMinimumSize(new Dimension(x, y));
		_dtField.setPreferredSize(new Dimension(w*2, h));
		_dtField.setMaximumSize(new Dimension(w*2, h));
		_dtField.setMinimumSize(new Dimension(w, h));
		
		// TODO: Limit input to numbers
		// SEE: https://docs.oracle.com/javase/10/docs/api/javax/swing/text/DocumentFilter.html
		
		ret.add(label);
		ret.add(_dtField);
				
		return ret;
	}
	
	private JComponent createSeparator() {
		JSeparator ret = new JSeparator(JSeparator.VERTICAL);
		ret.setMaximumSize(new Dimension(ret.getPreferredSize().width+SEPARATOR_PADDING, ret.getMaximumSize().height));
		
		return ret;
	}
	
	private void initGUI() {
		// Dejo la siguiente línea como recuerdo de que ya has puesto un maldito FlowLayout 3 veces y
		// no puedes poner pegamento en un maldito flowlayout
		// setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		// setBackground(Label.getBackground());
		// TODO: QUitar el maldito degradado
		// TODO: ¿Tal vez poner un estilo obligatorio con setui?
		
		add(_openButton = createButton("open.png", "Open file"));
		add(createSeparator());
		add(_gLawButton = createButton("physics.png", "Select gravity law"));
		add(createSeparator());
		add(_runButton = createButton("run.png", "Run simulation"));
		add(_stopButton = createButton("stop.png", "Stop simulation"));
		
		add(createDelayInput());
		add(createStepsInput());
		add(createDeltaTimeInput());
		
		add(Box.createHorizontalGlue());
		add(createSeparator());
		add(_exitButton = createButton("exit.png", "Exits the program"));
	}
	
	private void showGravityLawDialog() {
		// TODO as lambda expression
		
		// No hace falta hacer toString porque la interfaz lo hace automáticamente
		Factory<GravityLaws> glf = _ctrl.getGravityLawsFactory();
		ArrayList<GravityLaws> possible = new ArrayList<>();
		for (JSONObject jo : glf.getInfo()) {
			possible.add(glf.createInstance(jo));
		}
		
 		GravityLaws response = (GravityLaws) JOptionPane.showInputDialog(this, "Select gravity laws to be used.", "Gravity Laws Selector",
			JOptionPane.PLAIN_MESSAGE, null, possible.toArray(), _currentGravityLaw);
 		
 		if (response != null) _ctrl.setGravityLaws(response);
	}
	
	private void showFileDialog() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setCurrentDirectory(new File("."));
		int response = fileChooser.showOpenDialog(this);
		
		if (response == JFileChooser.APPROVE_OPTION) {
			_ctrl.reset();
			try (InputStream f = new FileInputStream(fileChooser.getSelectedFile())) {
				_ctrl.loadBodies(f);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Could not load file: " + e.getMessage(), 
						"Loading Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == _runButton) {
			try {
				_ctrl.setDeltatime(Double.valueOf(_dtField.getText()));
				int steps = (Integer)_stepsField.getValue();
				long delay = (Integer)_delayField.getValue();
				
				// This should not be executed if steps and delay fields are correctly configured
				if (steps < 0 || delay < 0) throw new IllegalArgumentException("Steps and delay should be greater than 0");
				
				_worker = new SimWorker(steps, delay);
				setButtonsEnabled(false);
				_worker.execute();
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "Couldn't start simulation: Steps and delta-time should be numbers",
						"Simulation Error", JOptionPane.ERROR_MESSAGE);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(this, "Could not start simulation: " + ex.getMessage(), 
						"Simulation error", JOptionPane.ERROR_MESSAGE);
			}
		} else if (e.getSource() == _stopButton) {
			// if (_thread != null) _thread.interrupt();
			if (_worker != null) _worker.cancel(true);
		} else if (e.getSource() == _exitButton) {
			int n = JOptionPane.showOptionDialog(this, "Are you sure you want to exit the program?", "Exit Window",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
					new String[] {"Yes", "No"}, "No");
			if (n == JOptionPane.YES_OPTION) System.exit(0);
		} else if (e.getSource() == _gLawButton) {
			showGravityLawDialog(); 
		} else if (e.getSource() == _openButton) {
			showFileDialog();
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
	private void setButtonsEnabled(boolean enabled) {
		_openButton.setEnabled(enabled);
		_gLawButton.setEnabled(enabled);
		_runButton.setEnabled(enabled);
		_stopButton.setEnabled(!enabled);
		_dtField.setEnabled(enabled);
		_stepsField.setEnabled(enabled);
		_delayField.setEnabled(enabled);
	}

	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawsDesc) {
		_dtField.setText(String.valueOf(dt));
		_currentGravityLaw = gLawsDesc;
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawsDesc) {
		onRegister(bodies, time, dt, gLawsDesc);
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		// We don't care
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		// Alias of onBodyAdded
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
		_dtField.setText(String.valueOf(dt));
	}

	@Override
	public void onGravityLawChanged(String gLawsDesc) {
		_currentGravityLaw = gLawsDesc;
	}

	private class SimWorker extends SwingWorker<Integer, Integer> {
		private int _n;
		private long _delay;
		
		public SimWorker(int n, long delay) {
			_n = n;
			_delay = delay;
		}

		/**
		 * Runs the simulation in the background
		 * @return The number of steps remaining to finish the simulation
		 */
		@Override
		protected Integer doInBackground() throws Exception {
			for (int i = 0; i < _n && !isCancelled(); ++i) {
				try {
					_ctrl.run(1);
				} catch (Exception e) {
					// Show the error in a dialog box
					setButtonsEnabled(true);
					JOptionPane.showMessageDialog(ControlPanel.this, "Simulation halted: " + e.getMessage(), "Simulation Warning", JOptionPane.WARNING_MESSAGE);
					return _n-i;
				}
				
				// With this if we don't have to publish every step,
				// only each "percentage"
				if (i%(_n/100) == 0)
					publish(100*i/_n + 1);
				
				Thread.sleep(_delay);
			}
			
			publish(100);
			return 0;
		}
		
		@Override
		protected void process(List<Integer> porcentajes) {
			if (_progressBar != null)
				_progressBar.setValue(porcentajes.get(porcentajes.size()-1));
		}
		
		@Override
		protected void done() {
			setButtonsEnabled(true);
		}

	}
}
