package simulator.model;

import org.json.JSONObject;

import simulator.misc.Vector;

public class Body {
	private String _id;
	
	protected Vector _x;
	protected Vector _v;
	protected Vector _a;
	
	protected double _m;
	
	public Body(String id, double mass, Vector x, Vector v) {
		if (x.dim() != v.dim()) throw new IllegalArgumentException();
		_id = id;
		_m = mass;
		_x = new Vector(x);
		_v = new Vector(v);
		_a = new Vector(x.dim());
	}
	
	public Body(String id, double m, double[] x, double[] v) {
		this(id, m, new Vector(x), new Vector(v));
	}

	// Copy constructor
	public Body(Body body) {
		this._id = body._id;
		this._x = new Vector(body._x);
		this._v = new Vector(body._v);
		this._a = new Vector(body._a);
		this._m = body._m;
	}
	
	/* Public getters */

	public String getId() {
		return _id;
	}
	
	public Vector getVelocity() {
		return new Vector(_v);
	}
	
	public Vector getAcceleration() {
		return new Vector(_a);
	}
	
	public Vector getPosition() {
		return new Vector(_x);
	}
	
	/* Package-private setters */
	
	public double getMass() {
		return _m;
	}
	
	void setVelocity(Vector v) {
		_v = new Vector(v);
	}
	
	void setPosition(Vector x) {
		_x = new Vector(x);
	}
	
	void setAcceleration(Vector a) {
		_a = new Vector(a);
	}
	
	void move(double t) {
		// x = x + v*t + a*t²/2
		_x = _x.plus(_v.scale(t).plus(_a.scale(.5*t*t)));
		_v = _v.plus(_a.scale(t));
	}
	
	public String toString() {
		return this.dump().toString();
	}
	
	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (this == obj) return true;

		// Usando getClass() mejor que instanceof
		if (getClass() != obj.getClass()) return false;
		
		// Safe casting
		Body bobj = (Body) obj;
		return _id.equals(bobj._id);
	}
	
	protected JSONObject getData() {
		JSONObject data = new JSONObject();
		data.put("id", _id);
		data.put("pos", _x.dump());
		data.put("vel", _v.dump());
		data.put("acc", _a.dump());
		data.put("mass", _m);
		return data;
	}

	public JSONObject dump() {
		JSONObject jo = new JSONObject();
		
		jo.put("type", "basic");
		jo.put("data", getData());
		return jo;
	}
	
}
