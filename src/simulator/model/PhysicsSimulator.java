package simulator.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class PhysicsSimulator {
	private GravityLaws gl;
	private double dt;
	private double t;
	private List<Body> bodies;
	private List<SimulatorObserver> observers;
	
	public PhysicsSimulator(GravityLaws gl, double tps) {
		this.gl = gl;
		this.dt = tps;
		this.t = 0;
		this.bodies = new ArrayList<>();
		this.observers = new ArrayList<>();
	}
	
	/** Aplica un paso de simulación, i.e., primero llama al método apply() de GravityLaws,
	 *  después llama a move(dt) para cada cuerpo, donde dt es el tiempo real por paso, y finalmente incrementa
	 *  el tiempo real en dt segundos */
	public void advance() {
		gl.apply(bodies);
		
		for (Body body : bodies) {
			body.move(dt);
		}
		
		t += dt;
		this.observers.forEach(o->o.onAdvance(bodies, t));
	}
	
	/** Añade el cuerpo b al simulador. el método debe comprobar que no existe ningún otro cuerpo en el simulador
	 * con el mismo identificador. Si existiera, el método debe lanzar una excepción del tipo IllegalArgumentException
	 * @param b Cuerpo único
	 */
	public void addBody(Body b) {
		// Nota: Se ha sobreescrito equals
		if (bodies.contains(b)) throw new IllegalArgumentException();
		else bodies.add(b);
		
		this.observers.forEach(o->o.onBodyAdded(bodies,b));
	}
	
	public String toString() {
		return dump().toString();
	}

	// Deberían estar PhysicsSimulator y Body implementados usando una interfaz
	// JSONDumpable o algo así?
	public JSONObject dump() {
		JSONObject dump = new JSONObject();
		dump.put("time", t);
		JSONArray jarray = new JSONArray();
		for (Body body : bodies) {
			jarray.put(body.getData());
		}
		dump.put("bodies", jarray);
		return dump;
	}
	
	/**
	 * Vacía la lista de cuerpos y pone el tiempo a 0,0
	 */
	public void reset() {
		this.bodies = new ArrayList<>();
		this.t = 0;
		this.observers.forEach(o->o.onReset(bodies,t,dt,gl.toString()));
	}
	
	/**
	 * Cambia el tiempo real por paso (deltatime) a dt.
	 * @param dt Debe ser mayor que 0
	 */
	public void setDeltaTime(double dt) {
		if (dt <= 0) throw new IllegalArgumentException("DeltaTime should be greater than 0");
		this.dt = dt;
		this.observers.forEach(o->o.onDeltaTimeChanged(dt));
	}
	
	/**
	 * Cambia las leyes de gravedad del simulador a gravityLaws
	 * @param gravityLaws
	 */
	public void setGravityLaws(GravityLaws gravityLaws) {
		if (gravityLaws == null) throw new IllegalArgumentException("Gravitylaws can't be null");
		this.gl = gravityLaws;
		this.observers.forEach(o->o.onGravityLawChanged(gravityLaws.toString()));
	}
	
	/**
	 * Añade o a la lista de observadores, si esque no está ya en ella
	 * @param o El observador que añadir al controlador
	 */
	public void addObserver(SimulatorObserver o) {
		if (observers.contains(o)) throw new IllegalArgumentException("Observer alredy in observers list");
		observers.add(o);
		o.onRegister(bodies, t, dt, gl.toString());
	}
}
