package simulator.factories;

import org.json.JSONObject;

import simulator.model.Body;

public class BasicBodyBuilder extends Builder<Body> {

	public BasicBodyBuilder() {
		super("basic", "Default body");
	}
	
	public BasicBodyBuilder(String typeTag, String desc) {
		super(typeTag, desc);
	}

	@Override
	protected JSONObject createData() {
		JSONObject data = new JSONObject();
		data.put("id", "the identifier");
		data.put("pos", "Position vector. Should be same dim as vel");
		data.put("vel", "Velocity vector. Should be same dim as pos");
		data.put("mass", "Mass of the object");
		return data;
	}
	
	public Body createTheInstance(JSONObject jo) {
		double[] x = jsonArrayTodoubleArray(jo.getJSONArray("pos"));
		double[] v = jsonArrayTodoubleArray(jo.getJSONArray("vel"));
		String id = jo.getString("id");
		double m = jo.getDouble("mass");
		
		return new Body(id, m, x, v);
	}
}
