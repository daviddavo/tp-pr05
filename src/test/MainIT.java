package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import simulator.launcher.Main;

class MainIT {

	// Make it only one string to test 1s, 2s... etc
	public static Stream<Arguments> testMainData() {
		return Stream.of(
			Arguments.of("out.1.txt", "resources/examples/ex1.2body.txt", "nlug", "-s 10000 -dt 10000"),
			Arguments.of("out.2.txt", "resources/examples/ex1.2body.txt", "ftcg", "-s 10000 -dt 10000"),
			Arguments.of("out.3.txt", "resources/examples/ex2.3body.txt", "nlug", "-s 10000 -dt 10000"),
			Arguments.of("out.4.txt", "resources/examples/ex2.3body.txt", "ftcg", "-s 10000 -dt 10000"),
			Arguments.of("out.5.txt", "resources/examples/ex3.4body.txt", "nlug", "-s 10000 -dt 10000"),
			Arguments.of("out.6.txt", "resources/examples/ex3.4body.txt", "ftcg", "-s 10000 -dt 10000"),
			Arguments.of("out.7.txt", "resources/examples/ex4.4body.txt", "nlug", "-s 10000 -dt 10000"),
			Arguments.of("out.8.txt", "resources/examples/ex4.4body.txt", "ftcg", "-s 10000 -dt 10000"),
			
			Arguments.of("out.1s.txt", "resources/examples/ex1.2body.txt", "nlug", "-s 10000"),
			Arguments.of("out.2s.txt", "resources/examples/ex1.2body.txt", "ftcg", "-s 10000"),
			Arguments.of("out.3s.txt", "resources/examples/ex2.3body.txt", "nlug", "-s 10000"),
			Arguments.of("out.4s.txt", "resources/examples/ex2.3body.txt", "ftcg", "-s 10000"),
			Arguments.of("out.5s.txt", "resources/examples/ex3.4body.txt", "nlug", "-s 10000"),
			Arguments.of("out.6s.txt", "resources/examples/ex3.4body.txt", "ftcg", "-s 10000"),
			Arguments.of("out.7s.txt", "resources/examples/ex4.4body.txt", "nlug", "-s 10000"),
			Arguments.of("out.8s.txt", "resources/examples/ex4.4body.txt", "ftcg", "-s 10000")
		);
	}
	
	// Mejora mucho la velocidad, pero sólo funcionará si la simulación sigue el mismo orden
	public static boolean goodSimilar(JSONObject o1, JSONObject o2) {
		boolean ret = true;
		
		JSONArray s1 = o1.getJSONArray("states");
		JSONArray s2 = o2.getJSONArray("states");
		
		if (s1.length() != s2.length()) return false;
		
		int i;
		for (i = 0; ret && i < s1.length(); ++i)
			ret = s1.getJSONObject(i).toString().equals(s2.getJSONObject(i).toString());
		
		if (!ret) {
			System.out.println("Diff at line: " + i);
			System.out.println("Exp: " + s1.getJSONObject(i));
			System.out.println("Gen: " + s2.getJSONObject(i));
		}
		
		return ret;
	}
	
	@ParameterizedTest(name = "o: {0}, i: {1}, gl: {2}")
	@MethodSource("testMainData")
	void testMain(String output, String input, String gl, String otherOptions) throws Exception {
		ArrayList<String> options = new ArrayList<>(Arrays.asList("-o", output, "-i", input, "-gl", gl));
		options.addAll(Arrays.asList(otherOptions.split(" ")));
		
		Main.main(options.toArray(new String[options.size()]));
		
		String expectedOutputFname = "./resources/output/" + output;
		JSONObject expectedOutput = new JSONObject(new JSONTokener(
				new FileInputStream(new File(expectedOutputFname))));
		JSONObject generatedOutput = new JSONObject(new JSONTokener(
				new FileInputStream(new File(output))));

		// Para ficheros mazo grandes, similar peta con una excepción
		// El código fuente de similar es una mierda y tiene un catch(Exception e) return false;
		// como una casa. Así que haremos nuestro propio similar
		
		assertTrue(goodSimilar(expectedOutput, generatedOutput));
		
		File file = new File(output);
		file.delete();
	}

}
